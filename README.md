# ARHead NEAR NFT contracts

* Standard: [NEP-171](https://nomicon.io/Standards/Tokens/NonFungibleToken/Core)

## Prerequisites

Get Rust in linux and MacOS

```sh
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
source $HOME/.cargo/env
```

Add the wasm toolchain

```sh
rustup target add wasm32-unknown-unknown
```

install dependencies (near-cli)

```sh
yarn
```

## Compile

```sh
yarn build
```

## Deploy

```sh
yarn near login
```

Once your logged in you have to deploy the contract. Make a subaccount with the name of your choosing

```sh
export NFT_CONTRACT_ID=nft-three.terferwxsagsbwqo.testnet
export MAIN_ACCOUNT=terferwxsagsbwqo.testnet
yarn near create-account -v $NFT_CONTRACT_ID --masterAccount $MAIN_ACCOUNT --initialBalance 10
```

Deploy the contract to that sub account

```sh
yarn near deploy -v --accountId $NFT_CONTRACT_ID --wasmFile out/main.wasm
```

Initialize deployed contract

```sh
yarn near call -v $NFT_CONTRACT_ID new_default_meta '{"owner_id": "'$NFT_CONTRACT_ID'"}' --accountId $NFT_CONTRACT_ID
```

Mint NFT

```sh
yarn near call $NFT_CONTRACT_ID nft_mint '{"token_id": "token-3", "metadata": {"title": "ArHead NFT token", "description": "The NFT token of ArHead", "media": "https://c.arhead.io/da138334-6b5a-4500-b16f-9563c6c0e087.png"}, "receiver_id": "'$MAIN_ACCOUNT'"}' --accountId $MAIN_ACCOUNT --amount 0.1
```

View NFTs

```sh
yarn near view -v $NFT_CONTRACT_ID nft_metadata
yarn near view $NFT_CONTRACT_ID nft_token '{"token_id": "token-1"}'
```
